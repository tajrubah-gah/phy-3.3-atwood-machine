﻿using System.Collections;
using UnityEngine;

public class ConnectInputGroup : MonoBehaviour
{
    protected InputGroup target;

    // Parameters:
    //   currentInput:
    //     The control that needs to receive user input
    public virtual void DeclareNumericInputField(InputGroup toSet){
        Debug.Log("setting input group of " + toSet.gameObject.name + " as target for " + this.gameObject.name);
        target = toSet;
    }
}

