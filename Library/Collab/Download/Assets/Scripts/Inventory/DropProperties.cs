﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropProperties : MonoBehaviour
{
    public bool isKinematicOnRelease, turnGravityOnOnRelease;
    public RigidbodyConstraints constraintsOnRelease;
    public float massOnRelease, zOnRelease, xOnRelease;
    public bool SnapsOnRelease, OnlyOneInstanceAllowed, RemainsDraggableOnRelease, CallDropHandler;

    public Transform constrainXBy;

    // // Start is called before the first frame update
    // void Start()
    // {
        
    // }

    // // Update is called once per frame
    // void Update()
    // {
        
    // }
}
