﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DragDropInventory : MonoBehaviour
{

    //Initialize Variables
    GameObject getTarget;
    // bool isMouseDragging;
    Vector3 offsetValue;
    Vector3 positionOfScreen;

    public GameObject[] Apparatus;
    public GameObject[] ApparatusImages;
    private bool ApparatusClicked;
    private int ApparatusIndex;
    // public Text CM;
    public string DragLayerName, TouchLayerName;

    private int cmdist;

    // Use this for initialization
    void Start()
    {
        PlayerPrefs.DeleteAll();
        ApparatusClicked = false;
        // CM.gameObject.SetActive(false);
    }

    void Update()
    {
        // if (GameObject.FindGameObjectWithTag("10"))
        // {
        //     ApparatusImages[0].SetActive(false);
        // }
        // else
        // {
        //     ApparatusImages[0].SetActive(true);
        // }

        // if (GameObject.FindGameObjectWithTag("MeterRod"))
        // {
        //     ApparatusImages[1].SetActive(false);
        // }
        // else
        // {
        //     ApparatusImages[1].SetActive(true);
        // }
        // if (GameObject.FindGameObjectWithTag("Ball"))
        // {
        //     ApparatusImages[2].SetActive(false);
        // }
        // else
        // {
        //     ApparatusImages[2].SetActive(true);
        // }

        // if (GameObject.FindGameObjectWithTag("Ball"))
        // {
        //     // if (ballController.Ginstance.rayHitting == true)
        //     // {
        //     //     CM.gameObject.SetActive(true);
        //     //     int temp = 0;
        //     //     temp = (int)meterRodController.Ginstance.dist;
        //     //     CM.text = temp.ToString() + " cm";
        //     // }
        //     // else
        //     // {
        //     //     CM.gameObject.SetActive(false);
        //     // }
        // }

        //Mouse Button Press Down
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hitInfo;

            if (ApparatusClicked == true && (!Apparatus[ApparatusIndex].GetComponent<DropProperties>().OnlyOneInstanceAllowed || 
                (Apparatus[ApparatusIndex].GetComponent<DropProperties>().OnlyOneInstanceAllowed && GameObject.Find(Apparatus[ApparatusIndex].gameObject.name+"(Clone)")==null)))
            {
                getTarget = Instantiate(Apparatus[ApparatusIndex]);
                Vector3 v3Temp = getTarget.transform.position;
                if(-99 != getTarget.GetComponent<DropProperties>().zOnRelease){
                    v3Temp.z = getTarget.GetComponent<DropProperties>().zOnRelease;
                }
                if(-99 != getTarget.GetComponent<DropProperties>().xOnRelease){
                    v3Temp.x = getTarget.GetComponent<DropProperties>().xOnRelease;
                }
                getTarget.transform.position = v3Temp;
                if(getTarget.GetComponent<DropProperties>().CallDropHandler && null!=getTarget.GetComponent<OnDrop>()){
                    getTarget.GetComponent<OnDrop>().HandleDrop();
                }
            }
            else
            {
                getTarget = ReturnClickedObject(out hitInfo);
            }

            if (getTarget != null)
            {
                if(null!=getTarget.GetComponent<Rigidbody>()){
                    getTarget.GetComponent<Rigidbody>().isKinematic = true;
                    getTarget.GetComponent<Rigidbody>().useGravity = false;
                }
                MarsiveAttack.Ginstance.isMouseDragging = true;
                //Converting world position to screen position.
                positionOfScreen = Camera.main.WorldToScreenPoint(getTarget.transform.position);
            }
        }

        //Mouse Button Up
        if (Input.GetMouseButtonUp(0))
        {
            if (getTarget != null)
            {
                if(null!=getTarget.GetComponent<Rigidbody>()){
                    getTarget.GetComponent<Rigidbody>().isKinematic = getTarget.GetComponent<DropProperties>().isKinematicOnRelease;
                    getTarget.GetComponent<Rigidbody>().useGravity = getTarget.GetComponent<DropProperties>().turnGravityOnOnRelease;

                    getTarget.GetComponent<Rigidbody>().constraints = getTarget.GetComponent<DropProperties>().constraintsOnRelease;

                    if (null!=getTarget.GetComponent<DropProperties>() && getTarget.GetComponent<DropProperties>().massOnRelease != -1)
                    {
                        getTarget.GetComponent<Rigidbody>().mass = getTarget.GetComponent<DropProperties>().massOnRelease;
                    }
                }

                if (null!=getTarget.GetComponent<DropProperties>() && getTarget.GetComponent<DropProperties>().SnapsOnRelease){
                    // check if dropping where it's supposed to snap
                    Transform SnapTo;
                    if(MarsiveAttack.Ginstance.IsTargetDroppingOnExpectedDestination(out SnapTo)){
                        // if yes, then snap it into the right place
                        SnapTo.GetComponent<SnapTo>().SnapIntoPlace();
                    }                    
                }

                ApparatusClicked = false;
                MarsiveAttack.Ginstance.isMouseDragging = false;
            }
        }

        //Is mouse Moving
        if (MarsiveAttack.Ginstance.isMouseDragging)
        {
            //tracking mouse position.
            Vector3 currentScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, positionOfScreen.z);

            // Debug.Log("screen space pos: (" + currentScreenSpace.x.ToString("0.###") + ", " +
            //             currentScreenSpace.y.ToString("0.###") + ", " + currentScreenSpace.z.ToString("0.###") + ")");

            //converting screen position to world position with offset changes.
            Vector3 currentPosition = Camera.main.ScreenToWorldPoint(currentScreenSpace);

            // Debug.Log("world space pos: (" + currentPosition.x.ToString("0.###") + ", " +
            //             currentPosition.y.ToString("0.###") + ", " + currentPosition.z.ToString("0.###") + ")");
            //float posX = 0.0f;
            if(null != getTarget.GetComponent<DropProperties>() && null != getTarget.GetComponent<DropProperties>().constrainXBy){
                currentPosition.x = getTarget.GetComponent<DropProperties>().constrainXBy.position.x;
            }
            else if(null != getTarget.transform.parent && 
                    null != getTarget.transform.parent.GetComponent<DropProperties>() && 
                    null != getTarget.transform.parent.GetComponent<DropProperties>().constrainXBy){
                currentPosition.x = getTarget.transform.parent.GetComponent<DropProperties>().constrainXBy.position.x;
                Debug.Log("anchored to x at: " + currentPosition.x.ToString("0.###"));
            }

            getTarget.transform.position = currentPosition;
            //It will update target gameobject's current postion.   
        }
    }

    //Method to Return Clicked Object
    GameObject ReturnClickedObject(out RaycastHit hit)
    {
        GameObject target = null;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray.origin, ray.direction, out hit))
        {
            Debug.Log("user tapped " + hit.collider.gameObject.name);
            if (hit.collider.gameObject.layer == LayerMask.NameToLayer(DragLayerName))
            {
                target = hit.collider.gameObject;
            }
        }
        return target;
    }

    public void ClickOnApparatus(int index)
    {
        ApparatusIndex = index;
        ApparatusClicked = true;
    }

    void OnCollisionEnter(Collision other)
    {
        Destroy(other.gameObject);
    }

    public void startButton()
    {
        cmdist = 0;
        if(GameObject.FindGameObjectWithTag("Ball"))
        {
            GameObject.FindGameObjectWithTag("Ball").GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            // cmdist = (int)meterRodController.Ginstance.dist;
            // stopWatchController.Ginstance.Actualstart = true;
        }
        // stopWatchController.Ginstance.start = true;
        
        
    }

    // public void saveButton()
    // {

    //     int No = 1;
    //     int flag = 0;
    //     if (PlayerPrefs.HasKey("No"))
    //     {
    //         No = PlayerPrefs.GetInt("No");
    //         No = No + 1;
    //         PlayerPrefs.SetInt("No", No);
    //     }
    //     else
    //     {
    //         PlayerPrefs.SetInt("No", No);
    //     }



    //     for (int i = 1; i <= PlayerPrefs.GetInt("No"); i++)
    //     {
    //         if (PlayerPrefs.HasKey("S[" + i + "]"))
    //         {
    //             if (PlayerPrefs.GetString("S[" + i + "]").ToString() == cmdist.ToString())
    //             {
    //                 if (!PlayerPrefs.HasKey("T2[" + i + "]"))
    //                 {
    //                     PlayerPrefs.SetString("T2[" + i + "]", stopWatchController.Ginstance.StopwatchTime.ToString("F2"));
    //                     No = No - 1;
    //                     flag = 1;
    //                 }
    //             }

    //         }
    //         else
    //         {
    //             if (flag == 0)
    //             {
    //                 PlayerPrefs.SetString("S[" + i + "]", cmdist.ToString());
    //                 PlayerPrefs.SetString("T1[" + i + "]", stopWatchController.Ginstance.StopwatchTime.ToString("F2"));
    //             }
    //         }
    //     }

    //     PlayerPrefs.SetInt("No", No);

    //     stopWatchController.Ginstance.ResetButton();

    // }
}