﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnDropMag : OnDrop
{
    [SerializeField] string zoomCameraName;
    [SerializeField] Transform lens;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void HandleDrop(){
        Debug.Log("handling drag, setting leader of " + zoomCameraName);
        GameObject zc  = GameObject.Find(zoomCameraName);
        if(null!=zc){
            zc.SetActive(true);
            zc.GetComponent<moveZoomCameraMag>().SetLeader(lens);
        }
    }
}
