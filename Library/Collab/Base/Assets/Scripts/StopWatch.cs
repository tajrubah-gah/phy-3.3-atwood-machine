﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StopWatch : MonoBehaviour {
private float sinceDropped;
private bool runStopWatch, SWEnabled;
[SerializeField] Text swText, swActualText;
[SerializeField] string strActualTimePrefix;
[SerializeField] UnityEngine.UI.Button btnStart, btnStop;
[SerializeField] Color deactivated, activatedSWText, activatedSWActualText;

	// Use this for initialization
	void Start () {
		sinceDropped = 0f;
		// swText = this.gameObject.transform.Find("StopWatchText").GetComponent<Text>();
		// swActualText = this.gameObject.transform.Find("ActualStopWatchText").GetComponent<Text>();
		runStopWatch = false;
		SWEnabled = false;
	}

	void UpdateSW(bool blnForce = false){
		System.TimeSpan t = System.TimeSpan.FromSeconds(sinceDropped);
		string answer = System.String.Format("{0:D2}.{1:D2}", 
			t.Seconds, 
			t.Milliseconds/10);
		swText.text = answer;
		// update actual time as well, unless...
		// 		m1 has touched down...
		// 		but even if m1 has touched down, if the blnForce flag is on, update!
		// blnForce is on when user clicks on Reset SW button
		if(!MarsiveAttack.Ginstance.HasTouchedDown() || blnForce){
			swActualText.text = this.strActualTimePrefix + "\n\n" + answer;
			swActualText.gameObject.SetActive(true);
		}
	}

	private void ActivateStopWatch(MarsiveState MarsiveStatus){
		// Debug.Log("marsive status: " + MarsiveStatus);
		if(MarsiveStatus==MarsiveState.swinging && this.SWEnabled){
			EnableStopWatch(false);
			this.SWEnabled = false;
		}
		else if(MarsiveStatus==MarsiveState.stabilised && !this.SWEnabled){
			EnableStopWatch(true);
			this.SWEnabled = true;
		}
	}

	private void EnableStopWatch(bool activateSW){
		btnStart.gameObject.SetActive(activateSW);
		btnStop.gameObject.SetActive(false);
		// swText.GetComponent<Text>().color = activateSW ? activatedSWText : deactivated;
		// swActualText.GetComponent<Text>().color = activateSW ? activatedSWActualText : deactivated;
	}
	
	void FixedUpdate () {

		ActivateStopWatch(MarsiveAttack.Ginstance.state);

		// if(runStopWatch){
		// 	sinceDropped += Time.fixedDeltaTime;
		// 	UpdateSW();
		// }
		// else
		// {
		// 	sinceDropped = 0f;
		// }
	}

	void Update () {

		if(runStopWatch){
			sinceDropped += Time.deltaTime;
			UpdateSW();
		}
		else
		{
			sinceDropped = 0f;
		}
	}

	public void ToggleSW(){
		if(MarsiveAttack.Ginstance.state==MarsiveState.swinging){
			return;
		}
		if(MarsiveAttack.Ginstance.currentObs.s<0){
			MarsiveAttack.Ginstance.GetComponent<ManageObs>().ActivatePanelForS(true);
			return;
		}
			runStopWatch = !runStopWatch;
		
	}

	public void ResetSW(){
		runStopWatch=false;
		sinceDropped = 0f;
		UpdateSW(true);
	}

	public bool isRunning(){
		return runStopWatch;
	}
}
