﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class DrawScale : MonoBehaviour {
public enum Handedness { LeftHanded = 0, RightHanded = 1 };
public Handedness handedness;

[SerializeField] Transform startAt, endAt, scaleObject, viewObject, markings;
[SerializeField] GameObject markSmallest, markMid, markTallest, lblCM;
[SerializeField] float margins;
[SerializeField] int size;
private const float _ONEMM = 1/1000f;

	// Use this for initialization
	void SetUpScale(){
		resizeScale(viewObject, size+margins);
		translateInX(startAt, size/(-2.0f));
		translateInX(endAt, size/(2.0f));
	}

	private void translateInX(Transform obj, float locX){
		Vector3 temp = obj.localPosition;
		temp.x = locX;
		obj.localPosition = temp;
	}

	private void resizeScale(Transform obj, float locX){
		Vector3 temp = obj.localScale;
		temp.x = locX;
		obj.localScale = temp;
	}

	// Update is called once per frame
	void Update () {
		
	}

	GameObject placeOneMark(Vector3 pos, GameObject markToPlace){
		GameObject mark = Instantiate(markToPlace, pos, Quaternion.identity, markings);
		if(this.handedness==Handedness.RightHanded){
			mark.GetComponent<MarkUtil>().moveRight();
		}
		return mark;
	}

	GameObject placeOneLabel(Vector3 pos, GameObject textToPlace, string strMsg){
		//Debug.Log(strMsg+": "+ pos);
		textToPlace.GetComponent<TMP_ScaleLabel2>().displayText = strMsg;
		textToPlace.GetComponent<TMP_ScaleLabel2>().Position.x = pos.x;
		if(this.handedness == Handedness.RightHanded){
			//textToPlace.GetComponent<TMP_ScaleLabel2>().yOffSet = -1*textToPlace.GetComponent<TMP_ScaleLabel2>().yOffSet;
			textToPlace.GetComponent<TMP_ScaleLabel2>().textAlignment = TMPro.TextAlignmentOptions.CaplineRight;
		}
		else{
			//textToPlace.GetComponent<TMP_ScaleLabel2>().Position.y = pos.y;
			textToPlace.GetComponent<TMP_ScaleLabel2>().textAlignment = TMPro.TextAlignmentOptions.CaplineLeft;
		}
		// Debug.Log(strMsg + ": " + textToPlace.GetComponent<TMP_ScaleLabel2>().Position);
		GameObject lbl = Instantiate(textToPlace, scaleObject.position, Quaternion.identity, markings);
		// Debug.Log(strMsg + ": " + textToPlace.GetComponent<TMP_ScaleLabel2>().Position);
		return lbl;
	}

	Vector3 drawBatch(Vector3 start, int count, float separation, GameObject markToPlace){
		
		Vector3 pos = start;
		for(int i=0; i<count; i++){
			// Debug.Log("pos vector: " + pos);
			placeOneMark(pos, markToPlace);
			pos.x += separation;
		}
		
		return pos;
	}

	Vector3 drawOneCM(Vector3 start, int label){
		placeOneLabel(start, lblCM, label.ToString());
		Vector3 tempNextStart = drawBatch(start, 1, _ONEMM, markTallest);
		tempNextStart = drawBatch(tempNextStart, 4, _ONEMM, markSmallest);
		tempNextStart = drawBatch(tempNextStart, 1, _ONEMM, markMid);
		return drawBatch(tempNextStart, 4, _ONEMM, markSmallest);
	}

	Vector3 drawOneMetre(Vector3 start, int metreIndex, bool printLast = false){
		Vector3 pos = start;
		int i;
		for(i = 0; i<100; i++){
			pos  = drawOneCM(pos, (metreIndex*100)+i);
			// Debug.Log((i+1) +": "+ pos);
		}
		if(printLast){
			placeOneMark(pos, markTallest);
			placeOneLabel(pos, lblCM, ((metreIndex*100)+i).ToString());
		}
		return pos;
	}

	void Start () {
		SetUpScale();
		Vector3 tempNextPos = startAt.position;
		int mi=0;
		while(mi<this.size){
			tempNextPos = drawOneMetre(tempNextPos, mi, (mi == (this.size-1)));
			mi++;
		}
		// placeOneMark(tempNextPos, markTallest);
		// placeOneLabel(tempNextPos, lblCM, (mi*100).ToString());
	}
	
}
