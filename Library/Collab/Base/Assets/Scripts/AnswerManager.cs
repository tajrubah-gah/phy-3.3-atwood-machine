﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnswerManager : MonoBehaviour
{
    public bool populated;
    public anstype type;
    public Opts MCs;
    [SerializeField] GameObject pnlOptions, pnlText, pnlNumeric, pnlBoolean;
    // Start is called before the first frame update
    void Start()
    {
        populated = false;
        Populate();
    }

    // Update is called once per frame
    void Update()
    {
        if(!populated){
            Populate();
        }        
    }
    public void ActivatePanel(GameObject pnlToActivate){
        pnlBoolean.SetActive(false);
        pnlText.SetActive(false);
        pnlNumeric.SetActive(false);
        pnlOptions.SetActive(false);
        pnlToActivate.SetActive(true);
    }

    public void Populate(){

        switch(type){
            case anstype.numeric:
                HandleNumeric();
                break;
            case anstype.mcq:
                HandleOptions();
                break;
            case anstype.boolean:
                HandleBoolean();
                break;
            case anstype.short_text:
                HandleText();
                break;
            case anstype.long_text:
                HandleText();
                break;
            default:
                populated = false;
                break;
        }
        populated = true;
    }

    private void HandleOptions(){
        Debug.Log("about to populate options");
        Debug.Log(MCs.GetOpts());
        ActivatePanel(pnlOptions);
        pnlOptions.GetComponent<OptionsManager>().optionSet = MCs;
        pnlOptions.GetComponent<OptionsManager>().Populate();
    }
    
    private void HandleNumeric(){
        ActivatePanel(pnlNumeric);
    }
    private void HandleText(){
        ActivatePanel(pnlText);
    }
    private void HandleBoolean(){
        ActivatePanel(pnlBoolean);
    }

    public void GetAnswer(Answer ans){
        switch(ans.type){
            case anstype.numeric:
                ans.ans_numeric = GetAnsNumeric();
                break;
            case anstype.mcq:
                ans.ans_option_index = GetAnsOption();
                break;
            case anstype.boolean:
                ans.ans_boolean = GetAnsBoolean();
                break;
            case anstype.short_text:
                ans.ans_short_text = GetAnsText();
                break;
            case anstype.long_text:
                ans.ans_long_text = GetAnsText();
                break;
            default:
                break;
        }
    }

    private float GetAnsNumeric(){
        return pnlNumeric.GetComponent<AnswerManagerNumeric>().Response();
    }

    private bool GetAnsBoolean(){
        return pnlBoolean.GetComponent<AnswerManagerBoolean>().Response();
    }

    private string GetAnsText(){
        return pnlText.GetComponent<AnswerManagerText>().Response();
    }

    private int GetAnsOption(){
        return pnlOptions.GetComponent<OptionsManager>().Response();
    }
    
}
