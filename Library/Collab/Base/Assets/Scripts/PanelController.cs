﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelController : MonoBehaviour
{

    public bool onlyS = false;
    [SerializeField] Vector2 posBtnOK;
    [SerializeField] InputGroup height, m1, m2, t;
    private ManageObs ObsManager;
    private Color bad = Color.red;

    private Color good;
    [SerializeField] GameObject btnOKHeight, btnOKAll, btnCancel;

    void Awake(){
        good = new Color(0.0f, 148.0f, 255.0f, 255.0f);
        ObsManager = MarsiveAttack.Ginstance.GetComponent<ManageObs>();
    }

    void OnEnable()
    {
        Debug.Log("currentObs: " + MarsiveAttack.Ginstance.currentObs.inspect());
        if(onlyS){
            height.Activate(true);
            m1.Activate(false);
            m2.Activate(false);
            t.Activate(false);
            btnOKAll.gameObject.SetActive(false);
            btnOKHeight.gameObject.SetActive(true);
        }    
        else{
            height.Activate(true);
            m1.Activate(true);
            m2.Activate(true);
            t.Activate(true);
            if(MarsiveAttack.Ginstance.currentObs.m1!=-1){
                m1.txt.text = (m1.GetBaseline()==-1) ? 
                                MarsiveAttack.Ginstance.currentObs.m1.ToString() : 
                                (MarsiveAttack.Ginstance.currentObs.m1 - m1.GetBaseline()).ToString();
                m1.UpdateFinal();
            }
            if(MarsiveAttack.Ginstance.currentObs.m2!=-1){
                m2.txt.text = (m2.GetBaseline()==-1) ? 
                                MarsiveAttack.Ginstance.currentObs.m2.ToString() : 
                                (MarsiveAttack.Ginstance.currentObs.m2 - m2.GetBaseline()).ToString();
                m2.UpdateFinal();
            }
            if(MarsiveAttack.Ginstance.currentObs.t!=-1){
                t.txt.text = MarsiveAttack.Ginstance.currentObs.t.ToString();
            }
            btnOKAll.gameObject.SetActive(true);
            btnOKHeight.gameObject.SetActive(false);
        }
        if(MarsiveAttack.Ginstance.currentObs.s!=-1){
            height.txt.text = MarsiveAttack.Ginstance.currentObs.s.ToString();
        }
        height.txt.Select(); height.txt.ActivateInputField();
    }

    public void SetSFromUI(){
        // float temp;
        // if(float.TryParse(height.txt.text, out temp)){
        //     ObsManager.SetS(temp);
        //     height.txt.GetComponentInChildren<Text>().color = good;
        //     height.lblErr.gameObject.SetActive(false);
        //     ObsManager.ActivatePanelForS(false);
        // }
        // else{
        //     height.txt.GetComponentInChildren<Text>().color = bad;
        //     height.lblErr.gameObject.SetActive(true);
        // }
        float tempObVal;
        if(SetObFromUI(height, out tempObVal)){
            if(tempObVal!=-1){
                ObsManager.SetS(tempObVal);
            }
            ObsManager.ActivatePanelForS(false);
        }
        Debug.Log("currentObs: " + MarsiveAttack.Ginstance.currentObs.inspect());
    }

    public bool SetObFromUI(InputGroup fromUI, out float obVal){
        
        if( (fromUI.lblFinal!=null && (obVal=fromUI.GetFinalVal())!=-1) ||
            (fromUI.lblFinal==null && float.TryParse(fromUI.txt.text, out obVal)) ){
            fromUI.txt.GetComponentInChildren<Text>().color = good;
            fromUI.lblErr.gameObject.SetActive(false);
            return true;
        }
        else{
            if(fromUI.txt.text=="" && fromUI.allowNull){
                obVal = -1;
                return true;
            }
            else{
                obVal = -1;
                fromUI.txt.GetComponentInChildren<Text>().color = bad;
                fromUI.lblErr.gameObject.SetActive(true);
                return false;
            }
        }
    }

    public void SetAllObsFromUI(){
        float tempObVal;
        bool allGood = true;
        if(SetObFromUI(height, out tempObVal)){
            if(tempObVal!=-1){
                ObsManager.SetS(tempObVal);
            }
        }
        else{
            allGood = false;
        }
        m1.UpdateFinal();
        if(SetObFromUI(m1, out tempObVal)){
            if(tempObVal!=-1){
                ObsManager.SetM1(tempObVal);
            }
        }
        else{
            allGood = false;
        }
        m2.UpdateFinal();
        if(SetObFromUI(m2, out tempObVal)){
            if(tempObVal!=-1){
                ObsManager.SetM2(tempObVal);
            }
        }
        else{
            allGood = false;
        }
        if(SetObFromUI(t, out tempObVal)){
            if(tempObVal!=-1){
               ObsManager.SetT(tempObVal);
            }
        }
        else{
            allGood = false;
        }
        if(allGood){
            ObsManager.ActivatePanelForAll(false);
        }
        Debug.Log("currentObs: " + MarsiveAttack.Ginstance.currentObs.inspect());
        MarsiveAttack.Ginstance.SaveObs(m1.GetBaseline(), m2.GetBaseline());
        MarsiveAttack.Ginstance.PrintPlayerPrefs();
    }

    // Update is called once per frame
    public void CancelObs()
    {
        MarsiveAttack.Ginstance.SetStateToGrounded();
    }
}
