﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ishtaaap : MonoBehaviour {

[SerializeField] float upperLimit;
private bool switchedGravityOff;
[SerializeField] GameObject MassHanger1, MassHanger2;

	// Use this for initialization
	void Start () {
		switchedGravityOff=false;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Debug.Log("m1.y: " + MassHanger1.transform.position.y + "m2.y: " + MassHanger2.transform.position.y);
		Debug.Log("is gravity switched off? " + switchedGravityOff);
		if(!switchedGravityOff && 
			(MassHanger1.transform.position.y > upperLimit || 
				MassHanger2.transform.position.y > upperLimit)){
			MassHanger1.GetComponent<Rigidbody>().isKinematic = 
				MassHanger2.GetComponent<Rigidbody>().isKinematic = true;
			MassHanger1.GetComponent<Rigidbody>().useGravity = 
				MassHanger2.GetComponent<Rigidbody>().useGravity = false;
				switchedGravityOff=true;

			// fix orientation to vertical
			Vector3 HangerRotation = MassHanger1.transform.rotation.eulerAngles;
			HangerRotation.z = 0f;
			MassHanger1.transform.rotation = Quaternion.Euler(HangerRotation);

			HangerRotation = MassHanger2.transform.rotation.eulerAngles;
			HangerRotation.z = 0f;
			MassHanger2.transform.rotation = Quaternion.Euler(HangerRotation);
		}
		else if(switchedGravityOff && needsToReverse()){
			MassHanger1.GetComponent<Rigidbody>().useGravity = 
				MassHanger2.GetComponent<Rigidbody>().useGravity = true;
			MassHanger1.GetComponent<Rigidbody>().isKinematic = 
				MassHanger2.GetComponent<Rigidbody>().isKinematic = false;
			switchedGravityOff=false;
		}
	}

	bool needsToReverse(){
		Debug.Log("m1.mass: " + MassHanger1.GetComponentInChildren<StackInfo>().GetTotalMass() +
					", m2.mass: " + MassHanger2.GetComponentInChildren<StackInfo>().GetTotalMass());
		if(MassHanger1.transform.position.y > MassHanger2.transform.position.y){
			if(MassHanger1.GetComponentInChildren<StackInfo>().GetTotalMass() > 
				MassHanger2.GetComponentInChildren<StackInfo>().GetTotalMass()){
					return true;
				}
		}
		else if(MassHanger1.transform.position.y < MassHanger2.transform.position.y){
			if(MassHanger1.GetComponentInChildren<StackInfo>().GetTotalMass() < 
				MassHanger2.GetComponentInChildren<StackInfo>().GetTotalMass()){
					return true;
				}
		}
		return false;
	}
}

